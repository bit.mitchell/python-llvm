import llvmlite.binding as llvm

# All these initializations are required for code generation!
llvm.initialize()
llvm.initialize_native_target()
llvm.initialize_native_asmprinter()  # yes, even this one

class Engine:

    def __init__(self):
        self._engine = self._create_execution_engine()


    def _create_execution_engine(self):
        """
        Create an ExecutionEngine suitable for JIT code generation on
        the host CPU.  The engine is reusable for an arbitrary number of
        modules.
        """
        # Create a target machine representing the host
        target = llvm.Target.from_default_triple()
        target_machine = target.create_target_machine()
        # And an execution engine with an empty backing module
        backing_mod = llvm.parse_assembly("")
        engine = llvm.create_mcjit_compiler(backing_mod, target_machine)
        return engine


    def _compile_ir(self, llvm_ir):
        """
        Compile the LLVM IR string with the given engine.
        The compiled module object is returned.
        """
        # Create a LLVM module object from the IR
        mod = llvm.parse_assembly(llvm_ir)
        mod.verify()

        # Now add the module and make sure it is ready for execution
        self._engine.add_module(mod)
        self._engine.finalize_object()
        self._engine.run_static_constructors()
        return mod


    def run(self, llvm_ir):
        self._compile_ir(llvm_ir)


        from ctypes import Structure, c_int64, c_double, CFUNCTYPE, POINTER, pointer, ARRAY

        class pod(Structure):
            _fields_ = [("x", c_int64),
                        ("y", c_int64),
                        ("z", c_int64)]


        # Run the function via ctypes
        # cfunc = CFUNCTYPE(c_int64, POINTER(pod), c_int64)(fptr)
        # x = pod(1, 2, 3)
        # res = cfunc(pointer(x), 3)
        # print("foo({{1, 2, 3}}, 3) =", res)

        fib_func = CFUNCTYPE(c_int64, c_int64)(self._engine.get_function_address("thing-fib-int"))
        for x in range(20):
            res = fib_func(x)
            print('Fib({}) = {}'.format(x, res))


        call_func = CFUNCTYPE(None, c_int64, POINTER(pod))(self._engine.get_function_address("thing-call-int"))
        res = pod(1, 2, 3)
        call_func(5, pointer(res))
        print('call(5) = {{{}, {}, {}}}'.format(res.x, res.y, res.z))

        ptr_test_func = CFUNCTYPE(c_int64, POINTER(c_int64), c_int64)(self._engine.get_function_address("thing-ptr_test-int*_int"))
        x = (c_int64*3)()
        r = ptr_test_func(x, 20)
        print('ptr_test(&x, 20) = {}; x = {}'.format(r, list(x)))
