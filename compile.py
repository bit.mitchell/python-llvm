from argparse import ArgumentParser

from parser.parser import Parser
from language.scanner import Scanner
from execution.engine import Engine

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('source', type=str, help='Source code file to be compiled')

    args = parser.parse_args()

    p = Parser()
    ptree = p.parse(args.source)

    s = Scanner()
    s.scan(ptree)

    s.compile()

    print('== LLVM IR ==')
    print(str(s.module))

    e = Engine()
    e.run(str(s.module))
