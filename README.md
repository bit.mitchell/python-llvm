


# Example code

```
// An example of the + operator
fn foo (x: double, y: double): double {
    return x + y;
}

// An example of the abs(...) function
fn bar (x: double): double {
    if (x < 0) {
        return -x;
    } else {
        return x;
    }
}

```


# Compiler Structure
The structure of the compiler is tricky. Language features must be built into
the abstract syntax tree from the start or they won't work. Keeping this simple
offers the best chance at keeping it maintainable.

Let us consider the following code:
```
fn foo(x: int, y: int): int {
    return x + y;
}
```
This should expand into the following AST:
```
Function "foo" ((int, int) -> int)
    - Return
        - Call "__add__" ((int, int) -> int)
            - #0
            - #1
```
where in this case, the `__add__` function of the `(int, int) -> int` type is a
machine function, which generates the `add` instruction.