
from ply import lex, yacc

######################
## Lexical Analysis ##
######################


tokens = [
    'PLUS', 'MINUS', 'DIVIDE', 'MULTIPLY', # math operators
    'EQUALS', # assignment
    'GT', 'LT', 'GTE', 'LTE', 'EQ', # comparisons
    'BAND', # bitwise logic (and address of)
    'LPAREN', 'RPAREN',
    'LBRACE', 'RBRACE',
    'LSQUARE', 'RSQUARE',
    'COLON', 'SEMICOLON',
    'COMMA', 'PERIOD',
    'INT', 'FLOAT',
    'NAME',
    'FUNCTION',
    'NAMESPACE',
    'STRUCT',
    'RETURN',
    'LET',
    'IF',
    'ELSE',
    #'newline'
]

t_PLUS = r'\+'
t_MINUS = r'\-'
t_DIVIDE = r'\/'
t_MULTIPLY = r'\*'
t_GT = r'\>'
t_LT = r'\<'
t_GTE = r'\>\='
t_LTE = r'\<\='
t_EQ = r'\=\='
t_BAND = r'\&'
t_EQUALS = r'\='
t_PERIOD = r'\.'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_LSQUARE = r'\['
t_RSQUARE = r'\]'
t_COLON = r'\:'
t_SEMICOLON = r'\;'
t_COMMA = r'\,'
t_NAME = r'[a-zA-Z_][a-zA-Z_0-9]*'
t_ignore = ' \t'


# a line comment goes from a // to a \n
def t_ignore_COMMENT(t):
    r'(?:\/\/[^\n]*|\/\*(?:(?!\*\/).|\n)*\*\/)'
    t.lexer.lineno += t.value.count('\n')

# eat any number of new lines but keep track of the line number
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_FLOAT(t):
    r'\d+\.\d+'
    t.value = float(t.value)
    return t

def t_FUNCTION(t):
    r'fn'
    return t

def t_NAMESPACE(t):
    r'namespace'
    return t

def t_RETURN(t):
    r'return'
    return t

def t_STRUCT(t):
    r'struct'
    return t

def t_LET(t):
    r'let'
    return t

def t_IF(t):
    r'if'
    return t

def t_ELSE(t):
    r'else'
    return t

def t_error(t):
    print("Illegal characters!")
    print(t)
    t.lexer.skip(1)


####################
## Syntax Parsing ##
####################


def p_error(p):
    print("Parse Error!")
    print(p)

def p_root(p):
    '''
    root : namespace
         | root namespace
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[2]]

def p_namespace(p):
    '''
    namespace : NAMESPACE NAME LBRACE namespace_body RBRACE
    '''
    p[0] = ('namespace', p[2], p[4])

def p_namespace_body(p):
    '''
    namespace_body : function
                   | struct
                   | namespace
                   | namespace_body namespace_body
                   | empty
    '''
    if len(p) == 2:
        p[0] = [p[1]] if p[1] is not None else []
    else:
        p[0] = p[1] + p[2]

def p_empty(p):
    '''
    empty :
    '''
    p[0] = None


def p_struct(p):
    '''
    struct : STRUCT NAME LBRACE member_list RBRACE
    '''
    p[0] = (p[1], p[2], p[4])

def p_member_list(p):
    '''
    member_list : member member_list
                | member
                | empty
    '''
    if len(p) == 3:
        p[0] = [p[1]] + p[2]
    else:
        p[0] = [p[1]] if p[1] is not None else []

def p_member(p):
    '''
    member : NAME COLON type SEMICOLON
    '''
    p[0] = (p[1], p[3])

def p_function(p):
    '''
    function : FUNCTION NAME LPAREN parameter_list RPAREN COLON type block
    '''
    p[0] = (p[1], p[2], p[4], p[7], p[8])

def p_parameter_list(p):
    '''
    parameter_list : parameter
                   | parameter COMMA parameter_list
                   | empty
    '''
    # this returns a list of (name, type) values
    if len(p) == 2:
        p[0] = [p[1]] if p[1] is not None else []
    else:
        p[0] = [p[1]] + p[3]

def p_parameter(p):
    '''
    parameter : NAME COLON type
    '''
    # this returns a tuple (name, type)
    p[0] = (p[1], p[3])

def p_block(p):
    '''
    block : LBRACE statement_list RBRACE
    '''
    # returns the statement list it captured
    p[0] = p[2]

def p_statement_list(p):
    '''
    statement_list : statement
                   | statement statement_list
                   | empty
    '''
    if len(p) == 2:
        p[0] = [p[1]] if p[1] is not None else []
    else:
        p[0] = [p[1]] + p[2]

def p_statement(p):
    '''
    statement : return SEMICOLON
              | let SEMICOLON
              | assign SEMICOLON
              | if
    '''
    p[0] = p[1]

def p_return(p):
    'return : RETURN expression'
    p[0] = ('return', p[2])

def p_let(p):
    '''
    let : LET NAME COLON type
        | LET NAME COLON type EQUALS expression
    '''
    if len(p) == 5:
        p[0] = ('let', p[2], p[4])
    else:
        p[0] = ('let', p[2], p[4], p[6])

def p_assign(p):
    '''
    assign : expression EQUALS expression
    '''
    p[0] = ('=', p[1], p[3])

def p_if(p):
    '''
    if : IF LPAREN expression RPAREN block
    '''
    p[0] = ('if', p[3], p[5])

def p_expression(p):
    '''
    expression : expression PLUS expression
               | expression MINUS expression
               | expression MULTIPLY expression
               | expression DIVIDE expression
               | expression GT expression
               | expression LT expression
               | expression GTE expression
               | expression LTE expression
               | expression EQ expression
               | expression PERIOD NAME
               | LPAREN expression RPAREN
               | MINUS expression %prec UMINUS
               | BAND expression %prec ADDRESSOF
               | MULTIPLY expression %prec DEREFERENCE
               | expression LSQUARE expression RSQUARE
               | call
               | name
               | int
               | float
    '''
    if len(p) == 2: # already parsed syntax
        p[0] = (p[1])
    elif len(p) == 3: # unary operations
        p[0] = (p[1] + 'u', p[2])
    elif len(p) == 4 and p[1] == '(' and p[3] == ')': # group rule
        p[0] = (p[2])
    elif len(p) == 5 and p[2] == '[' and p[4] == ']': # index rule
        p[0] = ('index', p[1], p[3])
    else: # general binary operation
        p[0] = (p[2], p[1], p[3])

def p_call(p):
    'call : NAME LPAREN argument_list RPAREN'
    p[0] = ('call', p[1], p[3])

def p_argument_list(p):
    '''
    argument_list : expression
                  | expression COMMA argument_list
                  | empty
    '''
    if len(p) == 2:
        p[0] = [p[1]] if p[1] is not None else []
    else:
        p[0] = [p[1]] + p[3]

def p_name(p):
    'name : NAME'
    p[0] = ('name', p[1])

def p_type(p):
    '''
    type : type_name
         | type_name MULTIPLY
         | type_name LSQUARE int RSQUARE
    '''
    if len(p) == 2:
        p[0] = ('normal', p[1]) # no pointer here
    elif len(p) == 3:
        p[0] = ('pointer', p[1]) # yeah, make it a pointer
    elif len(p) == 5:
        p[0] = ('array', p[1], p[3][1])

def p_type_name(p):
    '''
    type_name : NAME
              | type_name PERIOD NAME
    '''
    # return a list of names: ['ns1', 'ns2', 'Foo']
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[3]]

def p_int(p):
    'int : INT'
    p[0] = ('int', p[1])

def p_float(p):
    'float : FLOAT'
    p[0] = ('float', p[1])

precedence = (
    ('left', 'PLUS', 'MINUS'), # addition
    ('left', 'MULTIPLY', 'DIVIDE'), # multiplication
    ('right', 'UMINUS'),
    ('left', 'LT', 'GT', 'LTE', 'GTE', 'EQ'), # comparison
    ('right', 'ADDRESSOF', 'DEREFERENCE'),
    ('left', 'EQUALS'), # assignment
    ('left', 'PERIOD')
)



######################
## Parser Interface ##
######################


class Parser:
    def parse(self, filename):
        '''
        Parse the given filename into a parse tree. The parse tree is a
        collection of tuple nodes which describes the source code and is ready
        to be converted into an AST for compilation.
        '''

        with open(filename, 'r') as f:
            source = ''.join(f.readlines())

        l = lex.lex()
        l.input(source)

        p = yacc.yacc()
        return p.parse(lexer=l, tracking=True)
