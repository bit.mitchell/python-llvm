
from llvmlite import ir

from .value import Value, ValueType

class Block:

    def __init__(self, parent=None):
        self._statements = []
        self._locals = {}
        self._parent = parent
        self._builder = None


    def generate(self, b: ir.IRBuilder):
        for s in self._statements:
            s.generate(b)


    def append(self, statement):
        '''
        Append a statement to the block of code
        '''
        self._statements.append(statement)


    def allocate_local(self, name, ty):
        '''
        Allocate a new variable in the local stack scope with the given name and
        type
        '''
        if name in self._locals:
            raise Exception('Local variable {} already exists!'.format(name))

        ptr = self.builder.alloca(ty.machine_type, name=name)
        self._locals[name] = Value(ptr, ty, ValueType.Reference)


    def add_local(self, name, value):
        '''
        Add an existing variable to the local scope. I.e. a function argument
        '''
        self._locals[name] = value


    def resolve(self, name, fn_params=None):
        '''
        Attempt to resolve the given name into a Value or a Function. If
        resolving a function, pass in the function parameters (fn_params) as a
        list of Types so the overload resolution can occur.
        '''
        if name in self._locals:
            return self._locals[name]

        if self._parent:
            return self._parent.resolve(name, fn_params)

        return None


    @property
    def builder(self):
        return self._builder if self._builder else self._parent.builder


    @builder.setter
    def builder(self, value):
        self._builder = value


    def __str__(self):
        return '\n'.join([str(s) for s in self._statements])
