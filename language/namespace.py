
class Namespace:
    '''
    The Namespace class is a top level container of functions and global values.
    It can contain other namespaces, functions, and structs.
    '''

    def __init__(self, name, parent):
        'parent should be either a parent Namespace, or a Module'
        self._types = {}
        self._functions = {}
        self._namespaces = {}

        self._name = name
        self._parent = parent


    def add_function(self, fn):
        # note: we keep this is a dictionary of lists so we can do overloading
        if fn.name not in self._functions:
            self._functions[fn.name] = [fn]
        else:
            self._functions[fn.name].append(fn)
        return fn


    def add_type(self, ty):
        if ty.name in self._types:
            raise NameError("type {0} already exists".format(ty.name))

        self._types[ty.name] = ty
        return ty


    def add_namespace(self, ns):
        if ns.name in self._namespaces:
            raise NameError("namespace {} already exists".format(ns.name))

        self._namespaces[ns.name] = ns
        return ns


    def generate(self):
        'Generate all of the bytecode for this namespace'
        for ns in self._namespaces.values():
            ns.generate()

        for fn_list in self._functions.values():
            for fn in fn_list:
                fn.generate()


    def resolve_namespace(self, name):
        'Get the Namespace object for the given name'
        if name in self._namespaces:
            return self._namespaces[name]

        return self._parent.resolve_namespace(name)


    def resolve_type(self, ty):
        'Get the Type object for the given name'
        mod, names = ty[0], ty[1]
        # if we are only looking for a basic name this gets easier
        if len(names) == 1:
            if names[0] in self._types:
                t = self._types[names[0]]
                if mod == 'normal':
                    return t
                elif mod == 'pointer':
                    return t.pointer
                elif mod == 'array':
                    return t.get_array(ty[2])

            return self._parent.resolve_type(ty)

        # otherwise the name is qualified, we must look for namespaces instead
        ns = self.resolve_namespace(names[0])
        t = ns.resolve_type(('normal', names[1:]))
        if mod == 'normal':
            return t
        elif mod == 'pointer':
            return t.pointer
        elif mod == 'array':
            return t.get_array(ty[2])


    def resolve(self, name, argument_types=None): # when resolving a function, argument_types is required
        'Get the module level object stored under the given name'
        if name in self._functions:
            for fn in self._functions[name]:
                if fn.argument_types == argument_types:
                    return fn

        return self._parent.resolve(name, argument_types)


    @property
    def name(self):
        return self._name

    @property
    def full_name(self):
        if isinstance(self._parent, Namespace):
            return '{}.{}'.format(self._parent.full_name, self._name)
        else:
            return self._name

    @property
    def module(self):
        return self._parent.module
