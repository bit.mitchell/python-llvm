
from llvmlite import ir
from .value import Value, ValueType


class Expression:
    def build(self, b):
        raise RuntimeError("Expression has no code generator!")

    @property
    def type(self):
        raise RuntimeError("Expression has no type")



class Variable(Expression):
    def __init__(self, name, value):
        self._name = name
        self._value = value

    def build(self, b):
        return self._value

    @property
    def type(self):
        return self._value.type

    @property
    def value(self):
        return self._value

    def __str__(self):
        return self._name


class Literal(Expression):
    def __init__(self, ty, value):
        self._type = ty
        self._value = Value(ir.Constant(ty.machine_type, value), ty, ValueType.Literal)
        self._actual_value = value

    def build(self, b):
        return self._value

    @property
    def type(self):
        return self._type

    def __str__(self):
        return str(self._actual_value)



class Call(Expression):
    def __init__(self, func, args):
        self._fn = func
        self._args = args

    def build(self, b):
        return self._fn.call(b, self._args)

    @property
    def type(self):
        return self._fn.return_type

    def __str__(self):
        args = ', '.join(map(str, self._args))
        return '{0}({1})'.format(self._fn.name, args)


class Access(Expression):
    def __init__(self, lhs, index, member):
        self._lhs = lhs
        self._index = index
        self._member = member

    def build(self, b):
        # Get a reference to the lhs value
        lhs_reference = self._lhs.build(b)
        lhs_ptr = lhs_reference.get_rvalue()
        lhs_type = lhs_reference.type
        # determine the rhs type
        rhs_type = lhs_type.members[self._index][1]

        # get the indices for the GEP instruction
        array_index = ir.Constant(ir.IntType(32), 0)
        field_index = ir.Constant(ir.IntType(32), self._index)

        # determine the pointer to the rhs value
        rhs_ptr = b.gep(lhs_ptr, [array_index, field_index])
        # return a reference to it
        return Value(rhs_ptr, rhs_type, ValueType.Reference)

    @property
    def type(self):
        return self._member[1]

    def __str__(self):
        return '{0}.{1}'.format(self._lhs, self._member[0])


class AddressOf(Expression):
    def __init__(self, expr):
        self._expr = expr
        self._type = expr.type.pointer

    @property
    def type(self):
        return self._type

    def build(self, b):
        expr = self._expr.build(b)
        # an reference is just a sneaky pointer, all this does it treat it like
        # a literal pointer instead
        return Value(expr.get_rvalue(), self.type, ValueType.Literal)


class Dereference(Expression):
    def __init__(self, expr):
        self._expr = expr
        self._type = expr.type.pointee # expr should be a pointer

    @property
    def type(self):
        return self._type

    def build(self, b):
        expr = self._expr.build(b)
        # a reference is just a sneaky pointer, all this does is treat a literal
        # pointer as a reference instead.
        return Value(expr.get_lvalue(b), self.type, ValueType.Reference)


class Index(Expression):
    def __init__(self, array, index):
        self._array = array
        self._index = index

    @property
    def type(self):
        return self._array.type.pointee

    def build(self, b: ir.IRBuilder):
        return self._array.type.index_into(b, self._array, self._index)
