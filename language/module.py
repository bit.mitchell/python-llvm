from llvmlite import ir

class Module:
    '''
    The Module class is a top level container of all namespaces. It acts as the
    global namespace context and contains things like the integer types.
    '''

    def __init__(self):
        self._module = ir.Module()
        self._namespaces = {}
        self._functions = {} # this should be for MachineFunctions only
        self._types = {} # this should be for MachineTypes only


    def add_namespace(self, ns):
        if ns.name in self._namespaces:
            raise NameError('namespace {} already exists'.format(ns.name))

        self._namespaces[ns.name] = ns


    def add_function(self, fn):
        # note: we keep this is a dictionary of lists so we can do overloading
        if fn.name not in self._functions:
            self._functions[fn.name] = [fn]
        else:
            self._functions[fn.name].append(fn)
        return fn


    def add_type(self, ty):
        if ty.name in self._types:
            raise NameError("type {0} already exists".format(ty.name))

        self._types[ty.name] = ty
        return ty


    def generate(self):
        'Generate all of the bytecode for this module'
        for ns in self._namespaces.values():
            ns.generate()


    def resolve_namespace(self, name):
        if name in self._namespaces:
            return self._namespaces[name]

        raise NameError('Namespace {} doesn\'t exist'.format(name))


    def resolve_type(self, ty):
        # if we are only looking for a basic name this gets easier
        mod, names = ty[0], ty[1]

        if len(names) == 1:
            if names[0] in self._types:
                t = self._types[names[0]]
                if mod == 'normal':
                    return t
                elif mod == 'pointer':
                    return t.pointer
                elif mod == 'array':
                    return t.get_array(ty[2])

            raise NameError('Type {} doesn\'t exist'.format(names[0]))

        # otherwise the name is qualified, we must look for namespaces instead
        ns = self.resolve_namespace(names[0])
        t = ns.resolve_type(('normal', names[1:]))
        if mod == 'normal':
            return t
        elif mod == 'pointer':
            return t.pointer
        elif mod == 'array':
            return t.get_array(ty[2])


    def resolve(self, name, argument_types=None):
        if name in self._functions:
            for fn in self._functions[name]:
                if fn.argument_types == argument_types:
                    return fn

            raise NameError('Overloaded function \'{}\' doesn\' exist for the parameters: {}'.format(name, argument_types))

        raise NameError('Name {} doest\'t exist'.format(name))


    @property
    def module(self):
        return self._module


    def __str__(self):
        return repr(self._module)
