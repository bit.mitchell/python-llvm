
from llvmlite import ir

from .type import MachineType, StructType
from .value import Value, ValueType
from .block import Block

def mangle_name(ns, name, args):
    return "{}-{}-{}".format(ns.full_name, name, "_".join(map(str, args)))


class Function:
    def __init__(self, ns, name, args, ret):
        '''Initialize a new Function.
           args: a dictionary of name:type pairs indicating the function arguments'''
        self._args = args
        self._ret = ret
        self._type = MachineType.function(self.argument_types, ret, self.return_by_reference)
        self._ns = ns
        self._name = name

        self._function = ir.Function(ns.module, self._type.machine_type, self.mangled_name)

        entry = self._function.append_basic_block("__entry")
        self._builder = ir.IRBuilder(entry)

        self._block = Block(ns)
        self._block.builder = self._builder

        # all of the arguments must be defined in the local block
        for i, arg in enumerate(self._args):
            n, t = arg
            self._function.args[i].name = '__arg_{}'.format(n)

            if isinstance(t, MachineType):
                # literals must be copied into a local stack variable and be
                # turned into references
                ptr = self._builder.alloca(t.machine_type, name=n)
                self._builder.store(self._function.args[i], ptr)
                self._block.add_local(n, Value(ptr, t, ValueType.Reference))
            else:
                self._block.add_local(n, Value(self._function.args[i], t, ValueType.Reference))

        # set the return values name if needed
        if self.return_by_reference:
            self._function.args[-1].name = "__ret"

    @property
    def type(self):
        return self._type

    @property
    def return_type(self):
        return self._ret

    @property
    def argument_types(self):
        return [t for n, t in self._args]

    @property
    def return_by_reference(self):
        # return-by-reference should only happen for aggregate types
        return isinstance(self.return_type, StructType)

    @property
    def name(self):
        return self._name

    @property
    def block(self):
        return self._block

    @property
    def builder(self):
        return self._builder

    @property
    def function(self):
        return self._function

    @property
    def mangled_name(self):
        return mangle_name(self._ns, self.name, [t for n, t in self._args])


    def generate(self):
        print("Compiling {} ({}) ...".format(self.name, self.mangled_name), end='')
        self._block.generate(self._builder)
        print("done")


    def call(self, b: ir.IRBuilder, args):
        # step 1: prepare the arguments for the function. This means any
        # literals that fit in registers will be copied directly, and any
        # structs will be copied to the stack and passed by reference.
        arg_values = []
        for arg in args:
            value = arg.build(b)
            if isinstance(value.type, MachineType):
                arg_values.append(value.get_lvalue(b))
            else:
                # create some new storage on the stack and copy the struct over
                ptr = b.alloca(value.type.machine_type)
                src = value.get_rvalue()
                value.type.copy(b, src, ptr)
                arg_values.append(ptr)

        if self.return_by_reference:
            # if we are returning through a reference, allocate it on the stack
            # and pass the reference in. Then we can treat the result of this
            # function call like an r-value to that stack ptr.
            ret = b.alloca(self.return_type.machine_type, name='__res')
            b.call(self.function, arg_values + [ret])
            return Value(ret, self.return_type, ValueType.Reference)
        else:
            # If this is an ordinary function call, treat the return value like
            # an l-value with nothing fancy going on
            result = b.call(self.function, arg_values)
            return Value(result, self.return_type, ValueType.Literal)

    def do_return(self, b: ir.IRBuilder, expr):
        # Here we can handle our own return semantics
        if self.return_by_reference:
            return_value = expr.build(b).get_rvalue()
            return_dest = self._function.args[-1]
            self.return_type.copy(b, return_value, return_dest)
            b.ret_void()
        else:
            # if we are not returning by reference, then just return the lvalue
            # and call it a day
            b.ret(expr.build(b).get_lvalue(b))

    def __str__(self):
        args = ', '.join(['{0}: {1}'.format(n, t) for n, t in self._args])

        return 'fn {0} ({1}): {2} {{\n{3}\n}}'.format(
            self.name, args, self._ret, self._block)


#
# The MachineFunction is a special case of a regular Function in which it
# implements a specific op, i.e. mapping the __add__(int, int): int function to
# the i64 add (i64 a, i64 b) instruction.
#
class MachineFunction:
    def __init__(self, m, name, args, ret):
        '''Initialize a new machine Function.
           args: a list of types indicating the function arguments'''
        self._args = args
        self._ret = ret
        self._type = MachineType.function(args, ret)
        self._module = m
        self._name = name
        self.generator = lambda b, a: None

        self._module.add_function(self)

    @property
    def type(self):
        return self._type

    @property
    def return_type(self):
        return self._ret

    @property
    def argument_types(self):
        return self._args

    @property
    def name(self):
        return self._name

    @property
    def mangled_name(self):
        return mangle_name(None, self.name, [t for n, t in self._args])

    def generate(self, builder, args):
         # invoke the generator lambda
        return self.generator(builder, [a.build(builder) for a in args])

    def call(self, b, args):
        # Here we will handle how this "Function" is invoked by the call
        # expression. Note that all machine functions will return literals.
        return Value(self.generate(b, args), self.return_type, ValueType.Literal)

    def __str__(self):
        args = ', '.join(['{0}: {1}'.format(n, t) for n, t in self._args])
        return 'fn {0} ({1}): {2} (machine)'.format(self.name, args, self._ret)
