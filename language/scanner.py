
from .module import Module
from .type import StructType, MachineType
from .function import Function
from .block import Block
from .expression import *
from .statement import *
from .namespace import Namespace

class Scanner:

    def __init__(self):

        self._m = Module()

        boolean = self._m.add_type(MachineType.integer('bool', bits=1))
        integer = self._m.add_type(MachineType.integer('int', bits=64))
        double = self._m.add_type(MachineType.floating('double', double=True))

        f = MachineFunction(self._m, '__add__', [integer, integer], integer)
        f.generator = lambda b, a: b.add(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__add__', [double, double], double)
        f.generator = lambda b, a: b.fadd(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__sub__', [integer, integer], integer)
        f.generator = lambda b, a: b.sub(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__sub__', [double, double], double)
        f.generator = lambda b, a: b.fsub(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__mul__', [integer, integer], integer)
        f.generator = lambda b, a: b.mul(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__mul__', [double, double], double)
        f.generator = lambda b, a: b.fmul(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__div__', [integer, integer], integer)
        f.generator = lambda b, a: b.sdiv(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__div__', [double, double], double)
        f.generator = lambda b, a: b.fdiv(a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__gt__', [integer, integer], boolean)
        f.generator = lambda b, a: b.icmp_signed('>', a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__lt__', [integer, integer], boolean)
        f.generator = lambda b, a: b.icmp_signed('<', a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__gte__', [integer, integer], boolean)
        f.generator = lambda b, a: b.icmp_signed('>=', a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__lte__', [integer, integer], boolean)
        f.generator = lambda b, a: b.icmp_signed('<=', a[0].get_lvalue(b), a[1].get_lvalue(b))

        f = MachineFunction(self._m, '__eq__', [integer, integer], boolean)
        f.generator = lambda b, a: b.icmp_signed('==', a[0].get_lvalue(b), a[1].get_lvalue(b))


    @property
    def module(self):
        return self._m


    def scan(self, parse_tree):

        # populate all of the namespaces and struct names in the code
        for ns in parse_tree:
            self.scan_for_names(self._m, ns)

        # define all members in the structs
        for ns in parse_tree:
            self.populate_structs(self._m, ns)

        # now look for functions so they all exist
        for ns in parse_tree:
            self.scan_for_functions(self._m, ns)

        # now that all types and functions are defined, we can build the AST
        for ns in parse_tree:
            self.populate_functions(self._m, ns)


    def scan_for_names(self, parent, namespace):
        '''Scan through the given namespace looking for namespaces and types'''
        _, name, body = namespace
        ns = Namespace(name, parent)
        parent.add_namespace(ns)

        namespaces = [x for x in body if x[0] == 'namespace']
        structs = [x for x in body if x[0] == 'struct']

        # scan through any child namespaces
        for child in namespaces:
            self.scan_for_names(ns, child)

        # populate this namespace with any types
        for child in structs:
            _, name, _ = child
            ns.add_type(StructType(ns, name))


    def populate_structs(self, parent, namespace):
        _, name, body = namespace
        ns = parent.resolve_namespace(name)

        namespaces = [x for x in body if x[0] == 'namespace']
        structs = [x for x in body if x[0] == 'struct']

        # populate this namespace with any types
        for child in structs:
            _, name, members = child
            s: StructType = ns.resolve_type(('normal', [name]))

            for n, t in members:
                s.add_member(n, ns.resolve_type(t))

            s.finish()

        for child in namespaces:
            self.populate_structs(ns, child)


    def scan_for_functions(self, parent, namespace):
        _, name, body = namespace
        ns = parent.resolve_namespace(name)

        namespaces = [x for x in body if x[0] == 'namespace']
        functions = [x for x in body if x[0] == 'fn']

        for fn in functions:
            _, name, args, ret, _ = fn
            arg_list = [(n, ns.resolve_type(ty)) for n, ty in args]
            ns.add_function(Function(ns, name, arg_list, ns.resolve_type(ret)))

        for child in namespaces:
            self.scan_for_functions(ns, child)


    def populate_functions(self, parent, namespace):
        _, name, body = namespace
        ns = parent.resolve_namespace(name)

        namespaces = [x for x in body if x[0] == 'namespace']
        functions = [x for x in body if x[0] == 'fn']

        for fn in functions:
            _, name, args, _, body = fn
            arg_types = [ns.resolve_type(t) for n, t in args]

            f: Function = ns.resolve(name, arg_types)

            # compile the body of the function
            self.scan_block(ns, f, f.block, body)

        for child in namespaces:
            self.populate_functions(ns, child)


    def scan_expression(self, block, e):
        # map the binary operators to function names
        binary_ops = {
            '+': '__add__',
            '-': '__sub__',
            '*': '__mul__',
            '/': '__div__',

            '==': '__eq__',
            '>': '__gt__',
            '<': '__lt__',
            '>=': '__gte__',
            '<=': '__lte__',
        }

        unary_ops = {
        }

        if e[0] in binary_ops:
            a = self.scan_expression(block, e[1])
            b = self.scan_expression(block, e[2])
            return Call(block.resolve(binary_ops[e[0]], [a.type, b.type]), [a, b])

        elif e[0] in unary_ops:
            a = self.scan_expression(block, e[1])
            return Call(block.resolve(unary_ops[e[0]], [a.type]), [a])

        elif e[0] == '.':
            a = self.scan_expression(block, e[1])
            i = a.type.get_member_index(e[2])
            return Access(a, i, a.type.members[i])

        elif e[0] == '&u':
            return AddressOf(self.scan_expression(block, e[1]))

        elif e[0] == '*u':
            return Dereference(self.scan_expression(block, e[1]))

        elif e[0] == 'call':
            args = [self.scan_expression(block, x) for x in e[2]]
            fn = block.resolve(e[1], [a.type for a in args])
            return Call(fn, args)

        elif e[0] == 'index':
            array = self.scan_expression(block, e[1])
            index = self.scan_expression(block, e[2])
            return Index(array, index)

        elif e[0] == 'name':
            return Variable(e[1], block.resolve(e[1]))

        elif e[0] == 'int':
            return Literal(self._m.resolve_type(('normal', ['int'])), e[1])

        elif e[0] == 'float':
            return Literal(self._m.resolve_type(('normal', ['double'])), e[1])

        raise RuntimeError('Error parsing expression: ' + str(e))


    def scan_statement(self, ns, fn, block, s):
        if s[0] == 'return':
            return Return(fn, self.scan_expression(block, s[1]))

        elif s[0] == '=':
            a = self.scan_expression(block, s[1])
            b = self.scan_expression(block, s[2])
            return Assign(a, b)

        elif s[0] == 'let':
            name, ty = s[1], ns.resolve_type(s[2])
            block.allocate_local(name, ty)

            if len(s) == 4: # we are also doing an assign!
                lhs = Variable(name, b.resolve(name))
                return Let(name, ty, Assign(lhs, self.scan_expression(block, s[3])))

            return Let(name, ty)

        elif s[0] == 'if':
            cond = self.scan_expression(block, s[1])

            # create a new block with the appropriate parent
            b = Block(block)
            self.scan_block(ns, fn, b, s[2])

            return If(cond, b)

        raise RuntimeError('Error parsing statement: ' + str(s))


    def scan_block(self, ns, fn, block, statements):
        for s in statements:
            block.append(self.scan_statement(ns, fn, block, s))


    def compile(self):
        'Compile the module'
        self._m.generate()
