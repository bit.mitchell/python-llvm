

from llvmlite import ir
from .value import Value, ValueType

class MachineType:
    def __init__(self, machine_type, pointee=None):
        self._machine_type = machine_type
        self._name = str(self._machine_type)
        self._pointee = pointee

    @staticmethod
    def void():
        '''Create a new void type'''
        return MachineType(ir.VoidType())

    @staticmethod
    def integer(name, bits=64):
        '''Create a new integer type with the given number of bits'''
        t = MachineType(ir.IntType(bits))
        t.name = name
        return t

    @staticmethod
    def floating(name, double=True):
        '''Create a new floating point number, either single or double precision'''
        t = MachineType(ir.DoubleType() if double else ir.FloatType())
        t.name = name
        return t

    @staticmethod
    def function(args, ret, return_by_reference=False):
        '''Create a new function type, with the given arguments and return type'''
        arg_types = [a.machine_type if isinstance(a, MachineType) else a.pointer.machine_type for a in args]
        return_type = ret.machine_type

        if isinstance(ret, StructType):
            arg_types.append(ret.pointer.machine_type)
            return_type = ir.VoidType()

        ty = ir.FunctionType(return_type, arg_types)
        return MachineType(ty)

    @staticmethod
    def array(ty, length):
        '''Create a new array type which is a sequence of another type'''
        t = MachineType(ir.ArrayType(ty.machine_type, length))
        t.name = '{}[{}]'.format(ty.name, length)
        return t


    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value


    @property
    def machine_type(self):
        '''Get the internal LLVM type object'''
        return self._machine_type

    @property
    def pointer(self):
        '''Get the Type that represents a pointer to this type'''
        t = MachineType(ir.PointerType(self._machine_type), self)
        t.name = self.name + '*'
        return t

    @property
    def pointee(self):
        '''Get the Type that represents the pointed to type'''
        return self._pointee

    def get_array(self, length):
        '''Get the array type for this type with the given number of elements'''
        return ArrayType(self, length)


    def copy(self, b: ir.IRBuilder, src, dest):
        'Make a copy of this value from the src pointer to the dest pointer'
        b.store(b.load(src), dest)


    def index_into(self, b: ir.IRBuilder, array, index):
        # array should be a ptr, so get the address its pointing too
        ptr = array.build(b).get_lvalue(b)
        # calculate the appropriate index
        idx = b.trunc(index.build(b).get_lvalue(b), ir.IntType(32))
        # return the new address as a reference
        return Value(b.gep(ptr, [idx]), self, ValueType.Reference)


    def __eq__(self, other):
        '''Check if this type matches another'''
        return self.machine_type == other.machine_type and \
               self.name == other.name

    def __str__(self):
        return self._name



class StructType:
    def __init__(self, ns, name):
        self._name = name
        self._ns = ns
        self._type = None

        self._members = []
        self._type = ir.global_context.get_identified_type(self.full_name)
        self._type.name = self.full_name


    @property
    def name(self):
        return self._name

    @property
    def full_name(self):
        return '{}.{}'.format(self._ns.full_name, self.name)

    @property
    def machine_type(self):
        return self._type

    @property
    def pointer(self):
        t = MachineType(ir.PointerType(self.machine_type), self)
        t.name = self.name + '*'
        return t

    def get_array(self, length):
        '''Get the array type for this type with the given number of elements'''
        return ArrayType(self, length)

    @property
    def members(self):
        'Get the list of members in the [(name, type), ...] format'
        return self._members

    def get_member_index(self, name):
        'Get the index of the named member'
        return [n for n, t in self._members].index(name)

    def add_member(self, name, ty):
        'Add a new member to this type. Must be called before finish()'

        if name in self._members:
            raise NameError("Member {} already exists in struct {}".format(name, self.name))

        self._members.append((name, ty))

    def finish(self):
        'Finalise the structure of this type'
        self._type.set_body(*[x.machine_type for _, x in self._members])

    def copy(self, b: ir.IRBuilder, src, dest):
        'Make a copy of this struct from the src pointer to the dest pointer'
        zero = ir.Constant(ir.IntType(32), 0)
        for i, m in enumerate(self.members):
            index = ir.Constant(ir.IntType(32), i)
            _, t = m
            s = b.gep(src, (zero, index))
            d = b.gep(dest, (zero, index))
            t.copy(b, s, d)

    def __eq__(self, other):
        return self.full_name == other.full_name and self.machine_type == other.machine_type

    def __str__(self):
        return self._name

    def __repr__(self):
        members = '\n'.join(['{}: {};'.format(n, t) for n, t in self._members])
        return "struct {} {{\n{}\n}}".format(self.name, members)



class ArrayType:
    def __init__(self, ty, length):
        self._type = ty
        self._length = length
        self._machine_type = MachineType.array(ty, length)

    @property
    def name(self):
        return str(self)

    @property
    def machine_type(self):
        return self._machine_type.machine_type

    @property
    def pointer(self):
        t = MachineType(ir.PointerType(self.machine_type), self)
        t.name = self.name + '*'
        return t

    @property
    def pointee(self):
        return self._type

    def get_array(self, length):
        '''Get the array type for this type with the given number of elements'''
        return ArrayType(self, length)

    def copy(self, b: ir.IRBuilder, src, dest):
        zero = ir.Constant(ir.IntType(32), 0)
        for i in range(self._length):
            index = ir.Constant(ir.IntType(32), i)
            s = b.gep(src, (zero, index))
            d = b.gep(dest, (zero, index))
            self._type.copy(b, s, d)

    def index_into(self, b: ir.IRBuilder, array, index):
        ptr = array.build(b).get_rvalue() # this is a pointer to this ArrayType
        # calculate the appropriate indices
        zero = ir.Constant(ir.IntType(32), 0)
        idx = b.trunc(index.build(b).get_lvalue(b), ir.IntType(32))
        # return the new address as a reference
        return Value(b.gep(ptr, [zero, idx]), self, ValueType.Reference)


    def __eq__(self, other):
        return self.machine_type == other.machine_type

    def __str__(self):
        return '{}[{}]'.format(self._type.name, self._length)