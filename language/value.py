from enum import Enum

ValueType = Enum('ValueType', 'Literal Reference')

class Value:
    def __init__(self, value, ty, value_type):
        self._value = value
        self._type = ty
        self._value_type = value_type

    @property
    def is_reference(self):
        return self._value_type == ValueType.Reference

    @property
    def is_literal(self):
        return self._value_type == ValueType.Literal

    @property
    def type(self):
        return self._type

    def get_lvalue(self, b):
        if self.is_literal:
            return self._value
        else:
            return b.load(self._value)

    def get_rvalue(self):
        if self.is_literal:
            raise Exception('Cannot use an lvalue as an rvalue')
        else:
            return self._value

    def __str__(self):
        return '{} {} {}'.format(self._value_type, self._type, self._value)
