
from llvmlite import ir

class Statement:
    def generate(self, b):
        raise RuntimeError("Statement has no code generator!")



class Return(Statement):
    def __init__(self, fn, expr):
        self._fn = fn
        self._expr = expr

    def generate(self, b):
        self._fn.do_return(b, self._expr)

    def __str__(self):
        return 'return {0};'.format(self._expr)


class Let(Statement):
    def __init__(self, name, ty, assign=None):
        self._name = name
        self._ty = ty
        self._assign = assign

    def generate(self, b):
        # let doesn't actually generate any code but the assignment might
        if self._assign:
            self._assign.generate(b)

    def __str__(self):
        if self._assign:
            return 'let {}: {} = {};'.format(self._name, self._ty, self._assign.rhs)
        else:
            return 'let {}: {};'.format(self._name, self._ty)



class Assign(Statement):
    def __init__(self, lhs, rhs):
        'lhs: Expression, rhs: Expression'
        self._lhs = lhs
        self._rhs = rhs
        self._type = lhs.type

        if lhs.type != rhs.type:
            raise Exception("Cannot assign two disparate types")

    @property
    def rhs(self):
        return self._rhs

    def generate(self, b: ir.IRBuilder):
        lhs = self._lhs.build(b)

        if not lhs.is_reference:
            raise Exception("Cannot assign to an l-value")

        rhs = self._rhs.build(b)

        if rhs.is_reference:
            self._type.copy(b, rhs.get_rvalue(), lhs.get_rvalue())
        else:
            b.store(rhs.get_lvalue(b), lhs.get_rvalue())

    def __str__(self):
        return '{} = {};'.format(self._lhs, self._rhs)


class If(Statement):
    def __init__(self, cond, true):
        '''
        cond: Expression the expression to be compared against zero
        true: Block the block of code to run if the condition is true
        '''
        self._cond = cond
        self._true = true

    def generate(self, b: ir.IRBuilder):
        # convert the condition expression down to a boolean
        cond_value = self._cond.build(b)

        mt = cond_value.type.machine_type

        if isinstance(mt, ir.IntType) and mt.width == 1:
            cond = cond_value.get_lvalue(b)
        elif isinstance(mt, ir.IntType):
            zero = ir.Constant(ir.IntType(64), 0)
            cond = b.icmp_signed('>', cond_value.get_lvalue(b), zero)
        elif isinstance(mt, ir.FloatType):
            zero = ir.Constant(ir.FloatType(), 0)
            cond = b.fcmp_signed('>', cond_value.get_lvalue(b), zero)
        elif isinstance(mt, ir.DoubleType):
            zero = ir.Constant(ir.DoubleType(), 0)
            cond = b.fcmp_signed('>', cond_value.get_lvalue(b), zero)

        # If statements simple check if their integer conditional is larger than
        # zero. Negative or zero values are treated as false.

        with b.if_then(cond):
            self._true.generate(b)


    def __str__(self):
        return 'if ({}) {{\n{}\n}}'.format(self._cond, self._true)